﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FMODUnity;
using UnityEngine.SceneManagement;

public class ScoopMinigameControlScript : MonoBehaviour {
    private const float HPBarHeight = 0.16f;
    private const float HPBarWidth = 2.96f;
    public GameObject shieldImage;
    public Text shieldText;
    public GameObject fuelImage;
    public Text fuelText;
    public float fuelCapacity;
    public Text timeText;
    public float time;
    public Canvas gameGuiScreen;
    public Canvas gameOverScreen;
    public GameObject sprite;
    public Text gameOverText;
    public ResourceHolder resource;


    public float fuelRemaining;
    public float axisValue;

    private float fuelCollected = 0;
    private Rigidbody2D rb;
    private float startingFuel;

	[FMODUnity.EventRef]
	public string FuelEvent;
	FMOD.Studio.EventInstance Fuel;
    private bool gameOver;
    private int hullLoss;



    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
        startingFuel = fuelRemaining;
        Debug.Log(MultipleControllerInputWrapper.GetNumControllers());

        gameGuiScreen.enabled = true;
        gameOverScreen.enabled = false;
        sprite.GetComponent<SpriteRenderer>().enabled = true;

		Fuel = FMODUnity.RuntimeManager.CreateInstance (FuelEvent);
        gameOver = false;
        hullLoss = 0;
		//Fuel.start ();
    }
    public float Truncate(float value, int digits)
    {
        double mult = Math.Pow(10.0, digits);
        double result = Math.Truncate(mult * value) / mult;
        return (float)result;
    }
    void RedrawText()
    {
        float shieldPercent = ((fuelRemaining / startingFuel) * 100);
        shieldText.text = "POWER REMAINING: " + Truncate(shieldPercent, 2) + "%";
        shieldImage.GetComponent<Image>().fillAmount = fuelRemaining / startingFuel;

        fuelText.text = "FUEL COLLECTED: " + Truncate(fuelCollected, 0) + " tons";
        fuelImage.GetComponent<Image>().fillAmount = HPBarWidth * (fuelCollected / fuelCapacity);

        timeText.text = "TIME: " + Truncate(time, 0);

    }
    // Update is called once per frame
    void Update() {

        
        if (!gameOver) {
            axisValue = MultipleControllerInputWrapper.GetVerticalAxis(3.0f);
            rb.AddForce(transform.up * axisValue * 100);
            fuelRemaining -= Math.Abs(axisValue);
            time -= Time.deltaTime;

            if (this.transform.position.y < -3)
            {
                fuelCollected += 0.05f;
            }
            else if (this.transform.position.y < -2)
            {
                fuelCollected += 0.03f;
            }
            else if (this.transform.position.y < -1)
            {
                fuelCollected += 0.02f;
            }
            else if (this.transform.position.y < 0)
            {
                fuelCollected += 0.01f;
            }
        }
        RedrawText();
        if (fuelRemaining < 0 || this.transform.position.y<-4 || time<0 || gameOver)
        {
            EndGame();
        }
        
        if (gameOver && Input.GetKey(KeyCode.JoystickButton0))
        {
            resource.FuelTons+=(int)Math.Ceiling(fuelCollected);
            resource.HullHP -= (int)hullLoss;
            SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
        }
		//Fuel.setParameterValue ("SunDistance", (float)fuelRemaining);
		//if (fuelRemaining < 0)
		//{
		//	Fuel.release ();
		//}
    }

    private void EndGame()
    {
        gameGuiScreen.enabled = false;
        gameOverScreen.enabled = true;
        sprite.GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<Rigidbody2D>().gravityScale = 0;
        transform.position = new Vector3(0, 100, -2);
        float finalFuel = Truncate(fuelCollected, 0);
        string endGameText;
        if (finalFuel > 20)
        {
            endGameText = "Excellent Piloting Skills! You collected:\n" + finalFuel + " tons of fuel";
            if (!resource.hasUssrTrophy)
                resource.hasUssrTrophy = true;

        } else if (fuelCollected > 10)
        {
            endGameText = "Showing bravery and skill, you collected:\n" + finalFuel + " tons of fuel";
        } else
        {
            endGameText = "Luckily, you barely escaped the flames with:\n" + finalFuel + " tons of fuel";
        }
        if (time > 40)
        {
            endGameText += "\nThe ship took 15 points of hull damage due to the early crash";
            hullLoss = 15;
        } else if (time > 10)
        {
            endGameText += "\nThe ship took 10 points of hull damage from solar radiation";
            hullLoss = 10;
        } else
        {
            endGameText += "\nThe ship took a mere 5 points of hull damage from heat";
            hullLoss = 5;
            if (!resource.hasUssrTrophy && !resource.hasMatryoshkaTrophy)
                resource.hasMatryoshkaTrophy = true;
        }
        
        gameOverText.text = endGameText;
        gameOver = true;
    }
}
