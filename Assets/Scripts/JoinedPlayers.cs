﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu( menuName = "ScriptableObject/JoinedPlayers")]
public class JoinedPlayers : ScriptableObject
{
    [SerializeField]
    private List<Player> players;

    public List<Player> Players
    {
        get
        {
            return players;
        }

        set
        {
            players = value;
        }
    }

    public void ResetPlayers()
    {
        Players = new List<Player>();
    }

    public bool IsAnyOneAlive()
    {
        return (players.Count(p => !p.IsDead) > 0);
    }

    public Player GetRandomPlayer()
    {
        List<Player> alivePlayers = players.Where(p => !p.IsDead).ToList();
        return alivePlayers[Random.Range(0, alivePlayers.Count())];
    }
}
