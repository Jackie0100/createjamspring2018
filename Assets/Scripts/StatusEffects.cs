﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/StatusEffect")]
public class StatusEffect : ScriptableObject
{
    [SerializeField]
    string _name;
    [SerializeField]
    [TextArea]
    string _effectMessage;
    [SerializeField]
    int damage;
    [SerializeField]
    PlayerEvent _event;

    public string statusEffectName
    {
        get
        {
            return _name;
        }

        set
        {
            _name = value;
        }
    }


    public string EffectMessage
    {
        get
        {
            return _effectMessage;
        }

        set
        {
            _effectMessage = value;
        }
    }

    public PlayerEvent Event
    {
        get
        {
            return _event;
        }

        set
        {
            _event = value;
        }
    }

    public void DamagePlayer(Player player)
    {
        player.TakeDamage(damage);
    }

    public string FormatStringWithPlayer(Player player)
    {
        return EffectMessage.Replace("%NAME%", player.name);
    }
}
