﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using FMODUnity;

[CreateAssetMenu(menuName = "ScriptableObject/GameEvent")]
public class GameEvent : ScriptableObject
{
    [SerializeField]
    string _title;
    [SerializeField]
    [TextArea]
    string _descriptionText;
    [SerializeField]
    int damage;
    [SerializeField]
    int allValue;
    [SerializeField]
    StatusEffect _statusEffect;
    [SerializeField]
    PlayerEvent _event;
    [SerializeField]
    AllPlayerEvent _allPlayerEvent;

	[SerializeField]
	[FMODUnity.EventRef]
	private string _eventSoundName;

    public string Title
    {
        get
        {
            return _title;
        }

        set
        {
            _title = value;
        }
    }

    public string DescriptionText
    {
        get
        {
            return _descriptionText;
        }

        set
        {
            _descriptionText = value;
        }
    }

    public PlayerEvent Event
    {
        get
        {
            return _event;
        }

        set
        {
            _event = value;
        }
    }

    public StatusEffect StatusEffect
    {
        get
        {
            return _statusEffect;
        }

        set
        {
            _statusEffect = value;
        }
    }

    public AllPlayerEvent AllPlayerEvent
    {
        get
        {
            return _allPlayerEvent;
        }

        set
        {
            _allPlayerEvent = value;
        }
    }

    public string FormatStringWithPlayer(Player player)
    {
        return DescriptionText.Replace("%NAME%", player.name);
    }

    public void DamagePlayer(Player player)
    {
        player.TakeDamage(damage);
    }
    public void HealPlayer(Player player)
    {
        player.Heal(damage);
    }
    public void AddKarma(Player player)
    {
        player.AddKarma(damage);
    }
    public void RemoveKarma(Player player)
    {
        player.RemoveKarma(damage);
    }
    public void RemoveStatusEffect(Player player)
    {
        player.RemoveStatusEffect(StatusEffect);
    }
    public void AddFood(Player player)
    {
        player.resources.Rations += damage;
    }
    public void LoseFood(Player player)
    {
        player.resources.Rations -= damage;
        if (player.resources.Rations < 0)
        {
            player.resources.Rations = 0;
        }
    }
    public void AddFuel(Player player)
    {
        player.resources.FuelTons += damage;
    }
    public void LoseFuel(Player player)
    {
        player.resources.FuelTons -= damage;
        if (player.resources.FuelTons < 0)
        {
            player.resources.FuelTons = 0;
        }
    }
    public void DamageShip(Player player)
    {
        player.resources.HullHP -= damage;
        if (player.resources.HullHP < 0)
        {
            player.resources.HullHP = 0;
        }
    }
    public void HealShip(Player player)
    {
        player.resources.HullHP += damage;
        if (player.resources.HullHP > 100)
        {
            player.resources.HullHP = 100;
        }
    }

    // #####################################
    // #            Joint Players          #
    // #####################################
    public void HealEveryone(JoinedPlayers joinedPlayers)
    {
        for (int i = 0; i < joinedPlayers.Players.Count; i++)
        {
            Player currentPlayer = joinedPlayers.Players[i];
            if (!currentPlayer.IsDead)
            {
                currentPlayer.Heal(allValue);
            }
        };
    }
    public void HurtEveryone(JoinedPlayers joinedPlayers)
    {
        for (int i = 0; i < joinedPlayers.Players.Count; i++)
        {
            Player currentPlayer = joinedPlayers.Players[i];
            if (!currentPlayer.IsDead)
            {
                currentPlayer.TakeDamage(allValue);
            }
        };
    }
    public void ApplyStatusEffectToPlayer(Player player)
    {
        player.ApplyStatusEffect(StatusEffect);
    }

	public void PlaySound (Player player)
	{
		FMODUnity.RuntimeManager.PlayOneShot(_eventSoundName);
	}
}

[System.Serializable]
public class PlayerEvent : UnityEvent<Player>
{

}

[System.Serializable]
public class AllPlayerEvent : UnityEvent<JoinedPlayers>
{

}