﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Minigame_Astroids_Rocks : MonoBehaviour {

	private Vector2 _posA = new Vector2(12.0f, 6.4f);
	private Vector2 _posB = new Vector2(12.0f, 2.4f);
	private Vector2 _posX = new Vector2(12.0f, -1.7f);
	private Vector2 _posY = new Vector2(12.0f, -5.7f);
    public Minigame_Astroids_Spaceship spaceship;
    double randX;
    int randY;
    public int randSpeed;

    // Use this for initialization
    void Start () {
        Pos();
    }

    void Pos()
    {
		randX = UnityEngine.Random.Range(0.0f, 50.0f) - 80.0f;
		randY = UnityEngine.Random.Range(1, 4);
		randSpeed = UnityEngine.Random.Range(1, 3) * 5;
		float posY;
		switch (randY)
		{
			case 1: posY = 6.4f;
				break;
			case 2: posY = 2.4f;
				break;
			case 3: posY = -1.7f;
				break;
			case 4: posY = -5.7f;
				break;
			default:posY = -5.7f;
				break;
		}
		this.transform.position = new Vector2((float)randX, posY);
    }
    
    // Update is called once per frame
    void Update () {
        if (Math.Abs((int)spaceship.transform.position.x - (int)this.transform.position.x )<4
            && Math.Abs((int)spaceship.transform.position.y - (int) this.transform.position.y)<1)
        {
            Pos();
            spaceship.HitByAsteroid();
        }
        this.transform.Translate(new Vector2(randSpeed, 0) * Time.deltaTime);
        if (this.transform.position.x > 20)
            Pos();
	}
}
