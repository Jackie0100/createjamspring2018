﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ChoiseSelect : MonoBehaviour
{
    public Image AButton;
    public Image BButton;
    public Image XButton;
    public Image YButton;

    [SerializeField]
    KeyCode[] _userInput = new KeyCode[8];
    [SerializeField]
    JoinedPlayers _players;

    public KeyCode[] UserInput
    {
        get
        {
            return _userInput;
        }

        set
        {
            _userInput = value;
        }
    }

    public bool IsAllInputRecieved
    {
        get
        {
            for (int i = 0; i < Input.GetJoystickNames().Length; i++)
            {
                if (_userInput[i] == KeyCode.None)
                {
                    return false;
                }
            }
            return true;
        }
    }

    public int PlayerVotedItem
    {
        get
        {
            if (_userInput.Where(c => c != KeyCode.None).Count() == 0)
            {
                return -1;
            }

            //All disagree - pick one random
            if (_userInput.Distinct().Count(k => k != KeyCode.None) == Input.GetJoystickNames().Count())
            {
                return ((int)(_userInput[UnityEngine.Random.Range(0, Input.GetJoystickNames().Count())]) % (int)KeyCode.JoystickButton0);
            }
            //All agree - Pick agreed upon
            if (_userInput.Distinct().Count(k => k != KeyCode.None) == 1)
            {
                return (((int)_userInput.Distinct().ToArray()[0]) % (int)KeyCode.JoystickButton0);
            }

            var groups = _userInput.Where(c => c != KeyCode.None).GroupBy(i => i).OrderBy(c=>c.Count());

            List<KeyCode> candidates = new List<KeyCode>();
            int highest = -1;

            foreach (var group in groups)
            {
                if (highest < group.Count())
                {
                    candidates.Clear();
                }
                else if (highest > group.Count())
                {
                    continue;
                }
                candidates.Add(group.Key);
                highest = group.Count();
            }
            return ((int)candidates[UnityEngine.Random.Range(0, candidates.Count)]) % (int)KeyCode.JoystickButton0;
        }
    }



    private void OnEnable()
    {
        _userInput = new KeyCode[8];
    }

    // Update is called once per frame
    void Update ()
    {
        for (int i = 0; i < Input.GetJoystickNames().Length; i++)
        {
            if (Input.GetKey(KeyCode.Joystick1Button0 + (i * 20)) && AButton.enabled)
            {
                _userInput[i] = KeyCode.JoystickButton0;
            }
            else if (Input.GetKey(KeyCode.Joystick1Button1 + (i * 20)) && BButton.enabled)
            {
                _userInput[i] = KeyCode.JoystickButton1;
            }
            else if (Input.GetKey(KeyCode.Joystick1Button2 + (i * 20)) && YButton.enabled)
            {
                _userInput[i] = KeyCode.JoystickButton2;
            }
            else if (Input.GetKey(KeyCode.Joystick1Button3 + (i * 20)) && XButton.enabled)
            {
                _userInput[i] = KeyCode.JoystickButton3;
            }
        }

        Debug.Log(PlayerVotedItem);
	}
    public void SetVisibility(bool A, bool B, bool Y, bool X)
    {
        AButton.enabled = A;
        BButton.enabled = B;
        YButton.enabled = Y;
        XButton.enabled = X;
    }
    public void SetButtonText(string aText = "", string bText = "", string xText="", string yText = "")
    {
        AButton.GetComponentInChildren<Text>().text = aText;
        BButton.GetComponentInChildren<Text>().text = bText;
        XButton.GetComponentInChildren<Text>().text = xText;
        YButton.GetComponentInChildren<Text>().text = yText;
    }
}
