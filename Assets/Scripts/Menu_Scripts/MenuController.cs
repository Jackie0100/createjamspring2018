﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour {
    public Canvas TitleScreen;
    public Canvas CharCreateScreen;
    public Canvas OptionsScreen;
    void Start () {
        TitleScreen.enabled = true;
        CharCreateScreen.enabled = false;
        OptionsScreen.enabled = false;
    }

    internal void OpenCharCreateMenu()
    {
        TitleScreen.enabled = false;
        CharCreateScreen.enabled = true;
        OptionsScreen.enabled = false;
    }
    internal void EndGame()
    {
        TitleScreen.enabled = false;
        CharCreateScreen.enabled = false;
        OptionsScreen.enabled = false;
    }

    internal void OpenOptionsMenu()
    {
        TitleScreen.enabled = false;
        CharCreateScreen.enabled = false;
        OptionsScreen.enabled = true;
    }

    internal void OpenTitleMenu()
    {
        TitleScreen.enabled = true;
        CharCreateScreen.enabled = false;
        OptionsScreen.enabled = false;
    }
}
