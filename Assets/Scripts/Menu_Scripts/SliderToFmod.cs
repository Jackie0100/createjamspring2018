﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderToFmod : MonoBehaviour {
	
	[FMODUnity.EventRef]
	public string RadiationEvent;
	FMOD.Studio.EventInstance Radiation;

	// Use this for initialization
	void Start () {
		Radiation = FMODUnity.RuntimeManager.CreateInstance(RadiationEvent);
		Radiation.start();
	}
	
	// Update is called once per frame
	void Update () {
		Radiation.setParameterValue("MusikSlider", 1);
	}
}
