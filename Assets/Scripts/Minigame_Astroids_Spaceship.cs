﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FMODUnity;
using UnityEngine.SceneManagement;

public class Minigame_Astroids_Spaceship : MonoBehaviour {
    public Animation anim;


    private Vector2 _posA = new Vector2(12.0f, 6.4f);
	private Vector2 _posB = new Vector2(12.0f, 2.4f);
	private Vector2 _posX = new Vector2(12.0f, -1.7f);
	private Vector2 _posY = new Vector2(12.0f, -5.7f);
    private ChoiseSelect choice;
    public Image HPBar;
    public Text HPText;
    public double result;
    private bool gameOver;
	public Canvas gameGuiScreen;
	public Canvas gameOverScreen;
    public Text EndText;
    public ResourceHolder resource;
    public GameObject button;

    public Text timeText;
    public float time;

    public int HPValue;
    private float HPMax;

	[FMODUnity.EventRef]
	public string MeteorEvent;
	[FMODUnity.EventRef]
	public string SpaceshipHitEvent;

    // Use this for initialization
    void Start() {
        gameOver = false;
		gameGuiScreen.enabled = true;
		gameOverScreen.enabled = false;
        this.transform.position = new Vector2(12.0f, 6.4f);
        choice = this.GetComponent<ChoiseSelect>();
        HPMax = HPValue;
        gameOver = false;
    }
    public void RedrawUI()
    {
        if(gameOver)
        {
            EndGame();
        }
        HPText.text = "HP Remaining: " + HPValue;
        HPBar.GetComponent<Image>().fillAmount = (HPValue/HPMax);
        timeText.text = "TIME: " + Truncate(time, 0);
    }
    public void HitByAsteroid()
    {
        HPValue -= 10;
		FMODUnity.RuntimeManager.PlayOneShot (MeteorEvent);
		FMODUnity.RuntimeManager.PlayOneShot (SpaceshipHitEvent);

    }
    public float Truncate(float value, int digits)
    {
        double mult = Math.Pow(10.0, digits);
        result = Math.Truncate(mult * value) / mult;
        return (float)result;
    }

    public void EndGame()
    {
		gameGuiScreen.enabled = false;
		gameOverScreen.enabled = true;
        this.transform.position = new Vector2(200, 200);
        button.GetComponent<SpriteRenderer>().enabled = false;
        gameOver = true;
		string txt = "";
		if (HPValue > 89)
		{
			txt = "WOW! What an amazing performance!\nYou should be proud!\nYou earned 20L vodka";
			resource.Rations += 20;
            if(!resource.hasVodkaTrophy)
            {
				resource.hasVodkaTrophy = true;
                if (resource.hasUssrTrophy && resource.hasMatryoshkaTrophy)
                    resource.hasJamTrophy = true;
            }
		}
		else if (HPValue > 49)
		{
			txt = "You survived! Well played.\nYou earned 10L vodka";
			resource.Rations += 10;
		}
		else if (HPValue > 9)
		{
			txt = "You hit a lot of asteroids, be aware next time,\nthis is not good for your the spaceship!\nYou earned 5L vodka";
			resource.Rations += 5;
		}
		else if (HPValue == 0)
		{
			txt = "You barely survived the asteroids, you really need to be careful!\nThe incident caused 5 bottles of vodka to break.";
			resource.Rations -= 5;
            if (!resource.hasVodkaTrophy)
            {
				resource.hasVodkaTrophy = false;
                if (resource.hasJamTrophy)
                    resource.hasJamTrophy = false;
            }
		}
        EndText.text = txt;
    }


    // Update is called once per frame
    void Update () 
    {
        if(!gameOver)
        {
			int key = choice.PlayerVotedItem;
			anim = GetComponent<Animation>();
			time -= Time.deltaTime;
			switch (key)
			{
				case 0:
					
					this.transform.position = _posA;
					break;
				case 1:
					
					this.transform.position = _posB;
					break;
				case 2:
					
					this.transform.position = _posX;
					break;
				case 3:
					
					this.transform.position = _posY;
					break;
			}
			RedrawUI();
        }
		if(result < 0.1 || HPValue == 0)
        {
			EndGame();
            gameOver = true;
        }
        if (gameOver && Input.GetKey(KeyCode.JoystickButton0))
        {
            SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
        }

        //this.transform.Translate(Vector2.up); //spaceship up
    }
}
