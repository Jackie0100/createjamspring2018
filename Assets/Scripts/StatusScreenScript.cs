﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusScreenScript : MonoBehaviour {
    public Text txtTitleAText;
    public Text txtBodyAText;

    public Text txtTitleBText;
    public Text txtBodyBText;

    public Text txtTitleCText;
    public Text txtBodyCText;

    public Text txtTitleDText;
    public Text txtBodyDText;

    public Image overlay;

    public Text txtTitleText;

    public JoinedPlayers players;
    public ResourceHolder resource;
    public Image vodkaTrophy;
    public Image ussrTrophy;
    public Image matryoshkaTrophy;
    public Image jamTrophy;

    // Use this for initialization
    void Start () {
        vodkaTrophy.enabled = resource.hasVodkaTrophy;
        ussrTrophy.enabled = resource.hasUssrTrophy;
        matryoshkaTrophy.enabled = resource.hasMatryoshkaTrophy;
        jamTrophy.enabled = resource.hasJamTrophy;
        Player PlayerA = players.Players[0];
        Player PlayerB = players.Players[1];
        Player PlayerC = players.Players[2];
        Player PlayerD = players.Players[3];

        string StatsA = PlayerA.StatusEffectsString();
        string StatsB = PlayerB.StatusEffectsString();
        string StatsC = PlayerC.StatusEffectsString();
        string StatsD = PlayerD.StatusEffectsString();

        DisplayText(
            PlayerA.CharacterName, PlayerA.CharacterName + " has " + PlayerA.Hp + " HP remaining. Their status effects are as follows:\n"+ StatsA,
            PlayerB.CharacterName, PlayerB.CharacterName + " has " + PlayerB.Hp + " HP remaining. Their status effects are as follows:\n" + StatsB,
            PlayerC.CharacterName, PlayerC.CharacterName + " has " + PlayerC.Hp + " HP remaining. Their status effects are as follows:\n" + StatsC,
            PlayerD.CharacterName, PlayerD.CharacterName + " has " + PlayerD.Hp + " HP remaining. Their status effects are as follows:\n" + StatsD
            );

        txtTitleText.text = "WELL DONE. YOU TRAVELLED: " + resource.DaysTravelled + " DAYS THROUGH THE VOID";
        
    }

    public void HideUI()
    {
        txtTitleAText.gameObject.SetActive(false);
        txtTitleBText.gameObject.SetActive(false);
        txtTitleCText.gameObject.SetActive(false);
        txtTitleDText.gameObject.SetActive(false);

        txtBodyAText.gameObject.SetActive(false);
        txtBodyBText.gameObject.SetActive(false);
        txtBodyCText.gameObject.SetActive(false);
        txtBodyDText.gameObject.SetActive(false);

        overlay.enabled = false;
    }
    public void DisplayText(string TitleA = "", string BodyTextA = "", string TitleB = "", string BodyTextB = "", string TitleC = "", string BodyTextC = "", string TitleD = "", string BodyTextD = "")
    {
        txtTitleAText.text = TitleA;
        txtTitleBText.text = TitleB;
        txtTitleCText.text = TitleC;
        txtTitleDText.text = TitleD;

        txtBodyAText.text = BodyTextA;
        txtBodyBText.text = BodyTextB;
        txtBodyCText.text = BodyTextC;
        txtBodyDText.text = BodyTextD;
    }
        // Update is called once per frame
        void Update () {
		
	}
}
