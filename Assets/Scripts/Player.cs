﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/Player")]
public class Player : ScriptableObject
{
    [SerializeField]
    float _hp;
    [SerializeField]
    float _karma;
    [SerializeField]
    List<StatusEffect> _statusEffects;
    [SerializeField]
    public ResourceHolder resources;
    string _name;
    PlayerClass _playerClass;

    public bool IsDead
    {
        get
        {
            return (_hp <= 0);
        }
    }

    public string CharacterName
    {
        get
        {
            return _name;
        }

        set
        {
            _name = value;
        }
    }

    public PlayerClass CharacterClass
    {
        get
        {
            return _playerClass;
        }

        set
        {
            _playerClass = value;
        }
    }

    public float Hp
    {
        get
        {
            return _hp;
        }

        set
        {
            _hp = value;
        }
    }

    public List<StatusEffect> StatusEffects
    {
        get
        {
            return _statusEffects;
        }

        set
        {
            _statusEffects = value;
        }
    }

    public float Karma
    {
        get
        {
            return _karma;
        }

        set
        {
            _karma = value;
        }
    }

    public void TakeDamage(int damage)
    {
        Hp -= damage;
    }
    
    public string StatusEffectsString()
    {
        string statusEffects="";
        for (int i = 0; i < StatusEffects.Count; i++)
        {
            StatusEffect currentEffect = StatusEffects[i];
            statusEffects += currentEffect.EffectMessage+", ";
        }
        if (statusEffects != "")
        {
            return statusEffects;
        } else
        {
            return "NONE.";
        }
    }
    public void ApplyStatusEffect(StatusEffect statusEffect)
    {
        StatusEffects.Add(statusEffect);
    }
    internal void RemoveStatusEffect(StatusEffect statusEffect)
    {
        StatusEffects.RemoveAll(c=>c.statusEffectName == statusEffect.name);
    }

    internal void Heal(int healedHP)
    {
        Hp += healedHP;
    }

    internal void AddKarma(int damage)
    {
        Karma += damage;
        if (Karma > 100)
        {
            Karma = 100;
        }
        if (Karma < -100)
        {
            Karma = -100;
        }
    }

    internal void RemoveKarma(int damage)
    {
        Karma -= damage;
        if (Karma < -100)
        {
            Karma = -100;
        }
        if (Karma > 100)
        {
            Karma = 100;
        }
    }


}
