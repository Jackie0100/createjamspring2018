﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public enum PlayerClass { Captain, Pilot, Chef, Engineer };
public class PlayerCreationScript : MonoBehaviour {
    public Text txtOutputField;
    //public CharacterManager charManager;
    public Dropdown classDropdown;
    public Text titleText;
    public Text currentCharacters;
    public ResourceHolder resource;
    public Player PlayerA;
    public Player PlayerB;
    public Player PlayerC;
    public Player PlayerD;
    [SerializeField]
    private JoinedPlayers players;

    private int characterNumber = 1;
    private string playerName;
    private PlayerClass playerClass;
    private int HP;
    private int classDropdownIndex;
    public Text classDropdownText;


    // Use this for initialization
    void Start () {
        players.Players = new List<Player>();
        classDropdown.value = 0;
        classDropdownIndex = 0;
        OnClassChange(classDropdown);
        resource.hasJamTrophy = false;
        resource.hasMatryoshkaTrophy = false;
        resource.hasUssrTrophy = false;
        resource.hasVodkaTrophy = false;
    }
    private string getClassName()
    {
        switch (playerClass)
        {
            case PlayerClass.Captain:
                return "Kaptain";
            case PlayerClass.Engineer:
                return "Engineer";
            case PlayerClass.Pilot:
                return "Pilot";
            case PlayerClass.Chef:
                return "Chef";
            default:
                return "Kaptain";
        }
    }
    private void RedrawText()
    {
        string className = getClassName();
        txtOutputField.text = "Name: " + playerName + "\nHP: "+HP+"\nInjuries: None\nClass: "+ className;
    }
    private void RedrawCharText()
    {
        List<Player> listPlayers = players.Players;
        string charListString = "kurrent kharacters:";
        for (int i = 0; i < listPlayers.Count; i++)
        {
            Player currentPlayer = listPlayers[i];
            charListString += "\n"+currentPlayer.CharacterName;
        }
        currentCharacters.text = charListString;
    }
    public void OnNameChange(InputField nameField)
    {
        playerName = nameField.text;
        RedrawText();
    }
    public void OnClassChange(Dropdown classField)
    {
        classDropdownIndex = classField.value;
        string className = classField.options[classDropdownIndex].text;
        switch (className)
        {
            case "Kaptain":
                playerClass = PlayerClass.Captain;
                HP = 100;
                break;
            case "Engineer":
                playerClass = PlayerClass.Engineer;
                HP = 120;
                break;
            case "Pilot":
                playerClass = PlayerClass.Pilot;
                HP = 90;
                break;
            case "Chef":
                playerClass = PlayerClass.Chef;
                HP = 110;
                break;
            default:
                playerClass = PlayerClass.Captain;
                break;
        }
        RedrawText();
    }
    public void NextPlayerButton()
    {
        Player newPlayer;
        switch (characterNumber)
        {
            case 1:
                newPlayer = PlayerA;
                break;
            case 2:
                newPlayer = PlayerB;
                break;
            case 3:
                newPlayer = PlayerC;
                break;
            case 4:
            default:
                newPlayer = PlayerD;
                break;

        }
        // save the player data
        newPlayer.Hp = HP;
        newPlayer.CharacterName = playerName;
        newPlayer.CharacterClass = playerClass;
        //newPlayer.resources = resource;
        newPlayer.Karma = 0;

        // casual sinning
        //charManager.AddCharacter(newPlayer);

        players.Players.Add(newPlayer);


        // reset the various stats and fields
        characterNumber++;
        if (characterNumber < 5)
        {
            // reset the dropdown
            classDropdown.options.RemoveAt(classDropdownIndex);
            classDropdown.value = 0;
            classDropdownText.text = classDropdown.options[0].text;
            classDropdownIndex = 0;
            OnClassChange(classDropdown);
            titleText.text = "Kreate a kharacter p" + characterNumber;
            playerName = "";

            RedrawText();
            RedrawCharText();
        } else
        {
            // move onto the game I guess
            resource.DaysTravelled = 0;
            resource.FuelTons = 30;
            resource.Rations = 45;
            resource.HullHP = 100;
            SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
        }
    }
	// Update is called once per frame
	void Update () {
		
	}
}
