﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;
public enum ButtonType
{
    PLAY=1,
    SETTINGS=2,
    EXIT=3,
    UNKNOWN=4,
}
public class ClickableObject : MonoBehaviour
{
    private GameObject menuControllerObject;
    public MenuController menuController;
    // Use this for initialization
    void Start() {

    }
    public void Play()
    {
        menuController.OpenCharCreateMenu();
    }
    public void Options()
    {
        menuController.OpenOptionsMenu();
    }
    public void  Exit() {
        menuController.EndGame();
        Application.Quit();
    }
    public void BackOptions()
    {
        menuController.OpenTitleMenu();
    }
}
