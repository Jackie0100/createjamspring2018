﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/EventList")]
public class EventList : ScriptableObject
{
    [SerializeField]
    List<GameEvent> gameEvents;

    public List<GameEvent> GameEvents
    {
        get
        {
            return gameEvents;
        }

        set
        {
            gameEvents = value;
        }
    }

    public GameEvent GetRandomEvent()
    {
        return gameEvents[Random.Range(0, gameEvents.Count)];
    }
}
