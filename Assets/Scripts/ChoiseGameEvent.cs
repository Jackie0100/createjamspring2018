﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;

[CreateAssetMenu( menuName = "ScriptableObject/ChoiseGameEvent")]
public class ChoiseGameEvent : GameEvent
{
    [Space(30)]
    [SerializeField]
    string[] _chooseOptions;
    [SerializeField]
    PlayerChoiseEvent _playerChoiseEvent;


    public string[] ChooseOptions
    {
        get
        {
            return _chooseOptions;
        }

        set
        {
            _chooseOptions = value;
        }
    }

    public PlayerChoiseEvent PlayerChoiseEvent
    {
        get
        {
            return _playerChoiseEvent;
        }

        set
        {
            _playerChoiseEvent = value;
        }
    }

    public void SpacePirates(ChoiseGameEvent gameevent, SpaceShipFlyControl spaceshipcontrol, int choise)
    {
        switch(choise)
        {
            case 0:
                spaceshipcontrol.SpeechBuble.DisplayText("Good Job!", "After a fierce battle in the deep abyss of kosmos, the Pirates were defeated by your might. Gain 16 Rations pillaged from their wreck.");
                spaceshipcontrol.Players.Players.ForEach(p => p.resources.Rations += 4);
                break;
            case 1:
                spaceshipcontrol.SpeechBuble.DisplayText("Drink!", "Vodka is good! The peace offering takes the pirates by surprise, and they leave you be. Sadly, they took the vodka.");
                spaceshipcontrol.Players.Players.ForEach(p => p.resources.Rations-=1);
                if (spaceshipcontrol.Players.Players[0].resources.Rations < 0)
                {
                    spaceshipcontrol.Players.Players[0].resources.Rations = 0;
                }
                break;
            case 2:
                spaceshipcontrol.SpeechBuble.DisplayText("Ouch!", "That wasn't such a great idea! You each got a beating on behalf of the pirates and only escaped thanks to some fancy flying in an asteroid field.");
                spaceshipcontrol.Players.Players.ForEach(p => p.TakeDamage(10));
                break;
            case 3:
                spaceshipcontrol.SpeechBuble.DisplayText("Flee", "The crew puts the ship into full silence mode, with even the lights on the bridge turned off to avoid giving away your position. The strategy worked, and the pirates pass you by.");
                break;
        }
    }
}

[System.Serializable]
public class PlayerChoiseEvent : UnityEvent<ChoiseGameEvent, SpaceShipFlyControl, int>
{

}