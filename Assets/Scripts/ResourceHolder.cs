﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
[CreateAssetMenu(menuName = "ScriptableObject/ResourceHolder")]
public class ResourceHolder : ScriptableObject
{
    [SerializeField]
    private int fuelTons;
    [SerializeField]
    private int rations;
    [SerializeField]
    private int hullHP;
    [SerializeField]
    private int daysTravelled;
    [SerializeField]
    private JoinedPlayers playerList;
    public bool hasVodkaTrophy;
    public bool hasMatryoshkaTrophy;
    public bool hasJamTrophy;
    public bool hasUssrTrophy;
    public int FuelTons
    {
        get
        {
            return fuelTons;
        }

        set
        {
            fuelTons = value;
        }
    }

    public int Rations
    {
        get
        {
            return rations;
        }

        set
        {
            rations = value;
        }
    }

    public int HullHP
    {
        get
        {
            return hullHP;
        }

        set
        {
            hullHP = value;
        }
    }

    public JoinedPlayers PlayerList
    {
        get
        {
            return playerList;
        }

        set
        {
            playerList = value;
        }
    }

    public int DaysTravelled
    {
        get
        {
            return daysTravelled;
        }

        set
        {
            daysTravelled = value;
        }
    }

    public bool IsDead()
    {
        return hullHP < 1;
    }
    public bool IsOutOfFuel()
    {
        return fuelTons < 1;
    }
    public bool IsOutOfRations()
    {
        return rations < 1;
    }
    public void AdvanceTime()
    {
        int players = playerList.Players.Count(c => !c.IsDead);
        rations -= players;
        fuelTons -= 1;
        if (fuelTons < 0)
        {
            fuelTons = 0;
        }
        if (rations < 0)
        {
            rations = 0;
        }
        DaysTravelled += 1;
    }
}
