﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public static class MultipleControllerInputWrapper
{
    public static float GetVerticalAxis(float coeff = 3.0f)
    {
        var y1 = Input.GetAxis("Vertical") * Time.deltaTime * coeff;
        var y2 = Input.GetAxis("Verticalp2") * Time.deltaTime * coeff;
        var y3 = Input.GetAxis("Verticalp3") * Time.deltaTime * coeff;
        var y4 = Input.GetAxis("Verticalp4") * Time.deltaTime * coeff;
        var y5 = Input.GetAxis("Verticalp5") * Time.deltaTime * coeff;
        var y6 = Input.GetAxis("Verticalp6") * Time.deltaTime * coeff;
        var y7 = Input.GetAxis("Verticalp7") * Time.deltaTime * coeff;
        var y8 = Input.GetAxis("Verticalp8") * Time.deltaTime * coeff;
        int players = GetNumControllers();
        float axisValue = (y1 + y2 + y3 + y4 + y5 + y6 + y7 + y8) / players;
        return axisValue;
    }
    public static float GetHorizontalAxis(float coeff = 3.0f)
    {
        var x1 = Input.GetAxis("Horizontal") * Time.deltaTime * coeff;
        var x2 = Input.GetAxis("Horizontalp2") * Time.deltaTime * coeff;
        var x3 = Input.GetAxis("Horizontalp3") * Time.deltaTime * coeff;
        var x4 = Input.GetAxis("Horizontalp4") * Time.deltaTime * coeff;
        var x5 = Input.GetAxis("Horizontalp5") * Time.deltaTime * coeff;
        var x6 = Input.GetAxis("Horizontalp6") * Time.deltaTime * coeff;
        var x7 = Input.GetAxis("Horizontalp7") * Time.deltaTime * coeff;
        var x8 = Input.GetAxis("Horizontalp8") * Time.deltaTime * coeff;
        int players = GetNumControllers();
        float axisValue = (x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8) / players;
        return axisValue;
    }
    public static int GetNumControllers()
    {
        Input.GetJoystickNames().ToList().ForEach(c=>Debug.Log(c));
        int num = Input.GetJoystickNames().ToList().Count(c => c != "");
        if (num < 1)
        {
            return 1;
        } else
        {
            return num;
        }
    }
}
