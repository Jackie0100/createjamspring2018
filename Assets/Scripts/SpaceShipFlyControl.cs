﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShipFlyControl : MonoBehaviour
{
    [SerializeField]
    EventList eventList;
    [SerializeField]
    JoinedPlayers players;
    [SerializeField]
    MainScreenUIController speechBuble;
    [SerializeField]
    ResourceHolder _shipResources;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(CheckForEvents());
    }

    // Update is called once per frame
    void Update()
    {

    }

    public bool waitingforuserinput = false;

    public ResourceHolder ShipResources
    {
        get
        {
            return _shipResources;
        }

        set
        {
            _shipResources = value;
        }
    }

    public MainScreenUIController SpeechBuble
    {
        get
        {
            return speechBuble;
        }

        set
        {
            speechBuble = value;
        }
    }

    public JoinedPlayers Players
    {
        get
        {
            return players;
        }

        set
        {
            players = value;
        }
    }

    IEnumerator CheckForEvents()
    {
        while (Players.IsAnyOneAlive())
        {

            while (speechBuble.isPaused)
            {
                yield return new WaitForEndOfFrame();
            }
            int roll = Random.Range(0, 100);
            if (roll < 10 && players.Players[0].resources.FuelTons < 4)
            {
                speechBuble.miniGameEvent = true;
                speechBuble.DisplayText("Hydrogen", "You are in dire need of fuel and so prepare the fuel scoop and embark on the dangerous mission of siphoning hydrogen from the surface of a star.");
                speechBuble.Pause();
                while (!Input.GetKey(KeyCode.JoystickButton0))
                {
                    yield return new WaitForEndOfFrame();
                }
                SpeechBuble.UnPause();
            }
            else if (roll > 90 && players.Players[0].resources.Rations < 4)
            {
                speechBuble.miniMetorGameEvent = true;
                speechBuble.DisplayText("Vodka", "The crew are so desparate for rations that they broke into the kaptain's barrel of rum and drank all the drain cleaner - We need to extract space vodka from the Asteroids!");
                speechBuble.Pause();
                while (!Input.GetKey(KeyCode.JoystickButton0))
                {
                    yield return new WaitForEndOfFrame();
                }
                SpeechBuble.UnPause();
            }
            else if (roll <= 20)
            {
                Player targetPlayer = Players.GetRandomPlayer();
                GameEvent targetGameEvent = eventList.GetRandomEvent();
                targetGameEvent = eventList.GameEvents[15];

                if (targetGameEvent is ChoiseGameEvent)
                {
                    SpeechBuble.DisplayOptions(targetGameEvent.Title, targetGameEvent.DescriptionText, ((ChoiseGameEvent)targetGameEvent).ChooseOptions);

                    waitingforuserinput = true;
                    //SpeechBuble.Pause();
                    StartCoroutine(WaitForUserInputsCallBack());
                    while (waitingforuserinput)
                    {
                        yield return new WaitForEndOfFrame();
                    }

                    (targetGameEvent as ChoiseGameEvent).PlayerChoiseEvent.Invoke((ChoiseGameEvent)targetGameEvent, this, SpeechBuble.choiceSelect.PlayerVotedItem);

                    yield return new WaitForSeconds(0.1f);

                    while (!Input.GetKey(KeyCode.JoystickButton0))
                    {
                        yield return new WaitForEndOfFrame();
                    }
                    SpeechBuble.UnPause();
                }
                else
                {
                    SpeechBuble.DisplayText(targetGameEvent.Title.ToUpper(), targetGameEvent.FormatStringWithPlayer(targetPlayer).ToUpper());
                    targetGameEvent.Event.Invoke(Players.GetRandomPlayer());
                    SpeechBuble.Pause();
                    while (!Input.GetKey(KeyCode.JoystickButton0))
                    {
                        yield return new WaitForEndOfFrame();
                    }
                    SpeechBuble.UnPause();
                }
            }
            SpeechBuble.AdvanceDate();
            yield return new WaitForSeconds(1);
        }
    }

    IEnumerator WaitForUserInputsCallBack()
    {
        float waitTimer = 0;
        while (waitTimer < 10)
        {
            if (SpeechBuble.choiceSelect.IsAllInputRecieved)
            {
                break;
            }
            waitTimer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        waitingforuserinput = false;
    }
}
