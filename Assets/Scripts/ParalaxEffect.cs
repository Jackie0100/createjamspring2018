﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParalaxEffect : MonoBehaviour, ISpawnEvent, IDespawnEvent
{
    [SerializeField]
    GameObject[] layers;
    [SerializeField]
    float[] scrollSpeed;
    [SerializeField]
    Vector2 translateDir;
    [SerializeField]
    DictionaryObjectPool objPool;

    List<GameObject> spawnedElements;

    public void OnDespawned(GameObject targetGameObject, ObjectPool sender)
    {
        spawnedElements.Remove(targetGameObject);
    }

    public void OnSpawned(GameObject targetGameObject, ObjectPool sender)
    {
        spawnedElements.Add(targetGameObject);
    }

    // Use this for initialization
    void Start ()
    {
        spawnedElements = new List<GameObject>();

        objPool = new DictionaryObjectPool();

        objPool.AddObjectPool("LAYER1", new ObjectPool(layers[0], this.transform, 1));
        objPool.AddObjectPool("LAYER2", new ObjectPool(layers[1], this.transform, 1));
        objPool.AddObjectPool("LAYER3", new ObjectPool(layers[2], this.transform, 1));
        objPool.AddObjectPool("LAYER4", new ObjectPool(layers[3], this.transform, 1));

        objPool["LAYER1"].OnObjectSpawn += OnSpawned;
        objPool["LAYER2"].OnObjectSpawn += OnSpawned;
        objPool["LAYER3"].OnObjectSpawn += OnSpawned;
        objPool["LAYER4"].OnObjectSpawn += OnSpawned;

        objPool["LAYER1"].OnObjectDeSpawn += OnDespawned;
        objPool["LAYER2"].OnObjectDeSpawn += OnDespawned;
        objPool["LAYER3"].OnObjectDeSpawn += OnDespawned;
        objPool["LAYER4"].OnObjectDeSpawn += OnDespawned;

        for (int i = -60; i <= 60; i += 60)
        {
            objPool["LAYER1"].Spawn(new Vector3(i, 0));
            objPool["LAYER2"].Spawn(new Vector3(i, 0));
            objPool["LAYER3"].Spawn(new Vector3(i, 0));
            objPool["LAYER4"].Spawn(new Vector3(i, 0));

            //objPool["LAYER1"].Spawn(new Vector3(i, -10));
            //objPool["LAYER2"].Spawn(new Vector3(i, -10));
            //objPool["LAYER3"].Spawn(new Vector3(i, -10));
            //objPool["LAYER4"].Spawn(new Vector3(i, -10));

            //objPool["LAYER1"].Spawn(new Vector3(i, 10));
            //objPool["LAYER2"].Spawn(new Vector3(i, 10));
            //objPool["LAYER3"].Spawn(new Vector3(i, 10));
            //objPool["LAYER4"].Spawn(new Vector3(i, 10));
        }
    }

    // Update is called once per frame
    void Update ()
    {
        for (int i = 0; i < objPool["LAYER1"].Count; i++)
        {
            Vector2 vec = objPool["LAYER1"][i].transform.position;
            if (vec.x >= 60)
            {
                objPool["LAYER1"][i].transform.position = new Vector2(-120 + vec.x, 0);
            }
        }
        for (int i = 0; i < objPool["LAYER2"].Count; i++)
        {
            Vector2 vec = objPool["LAYER2"][i].transform.position;
            if (vec.x >= 60)
            {
                objPool["LAYER2"][i].transform.position = new Vector2(-120 + vec.x, 0);
            }
        }
        for (int i = 0; i < objPool["LAYER3"].Count; i++)
        {
            Vector2 vec = objPool["LAYER3"][i].transform.position;
            if (vec.x >= 60)
            {
                objPool["LAYER3"][i].transform.position = new Vector2(-120 + vec.x, 0);
            }
        }
        for (int i = 0; i < objPool["LAYER4"].Count; i++)
        {
            Vector2 vec = objPool["LAYER4"][i].transform.position;
            if (vec.x >= 60)
            {
                objPool["LAYER4"][i].transform.position = new Vector2(-120 + vec.x, 0);
            }
        }

        //Moving Effects
        for (int i = 0; i < objPool["LAYER1"].Count; i++)
        {
            objPool["LAYER1"][i].transform.Translate(translateDir * scrollSpeed[0] * Time.deltaTime);
        }
        for (int i = 0; i < objPool["LAYER2"].Count; i++)
        {
            objPool["LAYER2"][i].transform.Translate(translateDir * scrollSpeed[1] * Time.deltaTime);
        }
        for (int i = 0; i < objPool["LAYER3"].Count; i++)
        {
            objPool["LAYER3"][i].transform.Translate(translateDir * scrollSpeed[2] * Time.deltaTime);
        }
        for (int i = 0; i < objPool["LAYER4"].Count; i++)
        {
            objPool["LAYER4"][i].transform.Translate(translateDir * scrollSpeed[3] * Time.deltaTime);
        }
    }
}
