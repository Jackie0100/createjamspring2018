﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainScreenUIController : MonoBehaviour {
    public Text txtTitleText;
    public Text txtLeftBodyText;
    public Text txtFullBodyText;
    public Text txtDateText;
    public Text txtPrompt;
    public Text txtOutputStats;
    public ResourceHolder resources;
    public ChoiseSelect choiceSelect;
    public bool isPaused;
    internal bool miniGameEvent;
    internal bool miniMetorGameEvent;
    private void Start()
    {
        //Display2Option("Eggman", "Hello world I have one or two things to say first of all blah bleg easter bunny fneegle fneegle egg egg EGG.","EGG","egg?");
        txtPrompt.enabled = false;
        HideUI();
    }
    public void Pause()
    {
        Time.timeScale = 0.1f;
        txtPrompt.enabled = true;
        isPaused = true;

    }

    public void UnPause()
    {
        Time.timeScale = 1f;
        txtPrompt.enabled = false;
        isPaused = false;
        HideUI();
        if (miniGameEvent)
        {
            SceneManager.LoadScene("Minigame_Space_Scoop", LoadSceneMode.Single);
            miniGameEvent = false;
        }
        if (miniMetorGameEvent)
        {
            SceneManager.LoadScene("Minigame_Astroids", LoadSceneMode.Single);
            miniGameEvent = false;
        }
    }

    public void HideUI()
    {
        GetComponentInChildren<Image>().enabled = false;
        txtTitleText.gameObject.SetActive(false);
        txtLeftBodyText.gameObject.SetActive(false);
        txtFullBodyText.gameObject.SetActive(false);
        choiceSelect.gameObject.SetActive(false);
        choiceSelect.SetButtonText();
    }
    public void DisplayText(string Title, string BodyText)
    {
        GetComponentInChildren<Image>().enabled = true;
        txtTitleText.gameObject.SetActive(true);
        txtFullBodyText.gameObject.SetActive(true);
        txtTitleText.text = Title;
        txtFullBodyText.text = BodyText;

        // ############################

        txtLeftBodyText.gameObject.SetActive(false);
        choiceSelect.gameObject.SetActive(false);
    }

    public void DisplayOptions(string Title, string BodyText, string[] options)
    {
        if (options.Length == 2)
        {
            Display2Option(Title, BodyText, options[0], options[1]);
        }
        else if (options.Length == 3)
        {
            Display3Option(Title, BodyText, options[0], options[1], options[2]);
        }
        else if (options.Length == 4)
        {
            Display4Option(Title, BodyText, options[0], options[1], options[2], options[3]);
        }
        else
        {
            Debug.LogError("Too many options to display... Showing 4...");
            Display4Option(Title, BodyText, options[0], options[1], options[2], options[3]);
        }
    }

    public void Display2Option(string Title, string BodyText, string AText,string BText)
    {

        GetComponentInChildren<Image>().enabled = true;
        txtTitleText.gameObject.SetActive(true);
        txtLeftBodyText.gameObject.SetActive(true);
        choiceSelect.gameObject.SetActive(true);
        choiceSelect.SetVisibility(true, true, false, false);
        choiceSelect.SetButtonText(AText, BText);
        txtTitleText.text = Title;
        txtLeftBodyText.text = BodyText;

        // ############################

        txtFullBodyText.gameObject.SetActive(false);
        
    }
    public void Display3Option(string Title, string BodyText, string AText, string BText, string YText)
    {
        GetComponentInChildren<Image>().enabled = true;
        txtTitleText.gameObject.SetActive(true);
        txtLeftBodyText.gameObject.SetActive(true);
        choiceSelect.gameObject.SetActive(true);
        choiceSelect.SetVisibility(true, true, true, false);
        choiceSelect.SetButtonText(AText, BText, YText);
        txtTitleText.text = Title;
        txtLeftBodyText.text = BodyText;

        // ############################

        txtFullBodyText.gameObject.SetActive(false);

    }
    public void Display4Option(string Title, string BodyText, string AText, string BText, string YText, string XText)
    {
        GetComponentInChildren<Image>().enabled = true;
        txtTitleText.gameObject.SetActive(true);
        txtLeftBodyText.gameObject.SetActive(true);
        choiceSelect.gameObject.SetActive(true);
        choiceSelect.SetVisibility(true, true, true, true);
        choiceSelect.SetButtonText(AText, BText, YText, XText);
        txtTitleText.text = Title;
        txtLeftBodyText.text = BodyText;

        // ############################

        txtFullBodyText.gameObject.SetActive(false);

    }
    public void CheckGameOver()
    {
        if (resources.HullHP < 1 || !resources.PlayerList.IsAnyOneAlive())
        {

        }
    }
    public void AdvanceDate()
    {
        string dateText = "YEAR "+(int)(resources.DaysTravelled / 356)+", DAY "+(int)(resources.DaysTravelled % 356);
        txtDateText.text = dateText;
        string outputStatsString = "FOOD: " + resources.Rations + " RATIONS\nFUEL: " + resources.FuelTons + " TONS\nHULL STRENGTH" + resources.HullHP + "%";
        txtOutputStats.text = outputStatsString;
        CheckGameOver();
        resources.AdvanceTime();
    }
    private void Update()
    {
        
    }
}
